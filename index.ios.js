/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
} from 'react-native';

import Video from 'react-native-video';

export default class KitchenSink extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>
                    Welcome to React Native!
                </Text>
            </View>
        );
    }
}

// FOR TESTING VIDEO
class AppTestVideo extends Component {
    constructor(props) {
        super(props);

        var {height, width} = Dimensions.get('window');

        this.state = {
            paused: true,
            end: false,
            time: 0,
            duration: 0,
            height: height,
            width: width,
        };

    }

    onPressSeek() {
        this.video.seek(3.1);
        this.setState({
            paused: false,
        });
    }
    
    onPressPlay() {
        var newState = {paused: !this.state.paused};
        if (this.state.end) {
            this.video.seek(0);
            newState.end = false;
        }
        this.setState(newState);

        this.animateTextView.scrollText(0, this.state.duration);
    }

    onLoad(e) {
        console.log(e);
        this.setState({
            duration: e.duration,
        });
    }

    onProgress(e) {
        this.setState({
            time: e.currentTime
        });

        if (parseInt(e.currentTime) > parseInt(this.state.time)) {
            this.animateTextView.scrollText(e.currentTime, this.state.duration);
        }
    }

    onEnd() {
        this.setState({
            end: true,
            paused: true,
        });
    }

    getSecondsFormat(value) {
        return (Math.round(value * 10) / 10).toFixed(1);
    }

    render() {

        let time = this.getSecondsFormat(this.state.time);
        let duration = this.getSecondsFormat(this.state.duration);

        return (
            <View style={styles.container}>
                <Text style={styles.header}>Video Player</Text>
                <Video
                    ref={(ref) => { this.video = ref; }}
                    paused={this.state.paused}
                    repeat={false}
                    resizeMode='contain'
                    progressUpdateInterval={100}
                    // loading files with RN Asset System
                    source={require('./assets/video/test_720x480_1mb.mp4')}
                    onLoad={this.onLoad.bind(this)}
                    onProgress={this.onProgress.bind(this)}
                    onEnd={this.onEnd.bind(this)}
                    style={styles.backgroundVideo}
                />

                <SimpleButton buttonText="Play" onPress={this.onPressPlay.bind(this)} />
                <SimpleButton buttonText="Seek3" onPress={this.onPressSeek.bind(this)} />

                <Text>End: {String(this.state.end)}, Paused: {String(this.state.paused)}</Text>
                <Text>Time: {time} / {duration}</Text>

                <AnimateTextView
                    windowWidth={this.state.width}
                    ref={(ref) => { this.animateTextView = ref; }}
                    />
            </View>
        );
    }
}

// FOR TESTING ANIMATION
class AnimateTextView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            left: new Animated.Value(this.props.windowWidth),
            highlightIndex: 0,
        };
    }

    // TODO: to figure out width of a component use onLayout()
    // refs https://facebook.github.io/react-native/docs/view.html#onlayout

    componentDidMount() {
        this.state.left.setValue(this.props.windowWidth);
    }

    scrollText(currValue, maxValue) {
        // NOTE: reverse the animation
        let offsetLeft = this.props.windowWidth / 2;
        Animated.spring(
            this.state.left,
            {
                toValue: (maxValue - currValue) / maxValue * this.props.windowWidth - offsetLeft,
                tension: 1,
            }
        ).start();

        let newHighlightIndex = parseInt(currValue);
        if (newHighlightIndex != this.state.highlightIndex) {
            this.setState({
                highlightIndex: newHighlightIndex,
            });
        }
    }

    render() {
        let text = ['hello', 'world', 'how', 'are', 'you?', 'this', 'is', 'great!'];
        let textComponents = text.map((o, i) => {
            let _styles = [styles.animatedText];
            if (i === this.state.highlightIndex) {
                _styles.push(styles.animatedTextHighlight);
            }
            return <Text style={_styles} key={i}>{o} </Text>;
        });
        return (
            <View style={{marginTop: 10}}>
                <Animated.Text style={{left: this.state.left, fontSize: 20, width: 1000}}>
                    {textComponents}
                </Animated.Text>
            </View>
        );
    }
}

class SimpleButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <Text style={styles.buttonText}>{this.props.buttonText}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },

    header: {
        marginTop: 30,
        textAlign: 'center',
        fontSize: 20,
    },

    backgroundVideo: {
        width: 320,
        height: 200,

        borderColor: 'red',
        borderWidth: 1,
    },

    buttonText: {
        fontWeight: 'bold',
        color: 'green',
    },

    animatedText: {
    },

    animatedTextHighlight: {
        fontWeight: 'bold',
    },
});

AppRegistry.registerComponent('KitchenSink', () => AppTestVideo);
